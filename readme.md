# Instructions

Navigate to the directory you want to use

```sh
git clone https://gitlab.com/vterzic/eth-project.git
cd eth-project
cp ./eth-server/.env-example ./eth-server/.env
docker-compose up -d
```

✅ Client is running on port **80**

✅ Server is running on port **3000**

#### Notes

##### client routes are utilize Etherscan

##### .env.example ⚠️

.env.example is intentionally prefilled so you don't have to bother with that

##### Getting balance on date ⚠️

This route isn't accessable via client, because of it's slowness.

It uses Infura.

If you want to try it you can hit it using a rest client with url this:

```
http://localhost:3000/eth/infura?address=0xaa7a9ca87d3694b5755f213b5d04094b8d0f0a6f&date=2021-01-24
```

If you want to see it working

```
docker-compose logs -f eth-server
```

## Usage without docker

If you want to use apps without docker

For client:

```sh
cd eth-clinet
npm i
npm start
```

For server:

```sh
cd eth-server
npm i
npm start
```
