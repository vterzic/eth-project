import { Component } from '@angular/core';
import { Transaction } from '../shared/models/transaction';
import { UiService } from '../shared/services/ui.service';
import { ApiResponse, ApiService } from './api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  transactions: Transaction[] = [];
  address = '';
  contract: string;
  balance: number;
  value: number;
  ethPrice: number;

  isLoading = false;
  offsetLoading = false;
  offset = 10;
  readonly gwei = 0.000000001;
  readonly wei = 0.000000000000000001;

  constructor(private apiService: ApiService, private uiService: UiService) {}

  ionViewWillEnter() {
    // should be a third party service call
    this.ethPrice = 1240.04;
  }

  async fetchData() {
    this.offset = 10;
    this.isLoading = true;
    const res = await this.apiService
      .getData(this.address, this.offset)
      .catch(() => {
        this.uiService.showToast('Etherscan problem occured ');
      });

    if (res) {
      if (!this.isValid(res)) {
        this.uiService.showToast('No data for address');
      } else {
        this.updateData(res);
      }
    }

    this.isLoading = false;
  }

  isValid(res: ApiResponse) {
    return (
      Number.parseInt(res.balance.status, 10) &&
      Number.parseInt(res.transaction.status, 10)
    );
  }

  async fetchMoreData() {
    this.offset += 10;
    this.offsetLoading = true;
    const res = await this.apiService
      .getData(this.address, this.offset)
      .catch((err) => {
        console.error(err);
      });

    if (res) {
      const { transaction } = res;
      this.transactions.push(...transaction.result);
    }

    this.offsetLoading = false;
  }

  private updateData(data: ApiResponse) {
    this.contract = this.address;
    this.balance = Number.parseInt(data.balance.result) * this.wei;
    this.value = this.balance * this.ethPrice;
    this.transactions = data.transaction.result;
  }

  getTxnFee(t: Transaction) {
    return (
      Number.parseFloat(t.gasPrice) *
      Number.parseFloat(t.gasUsed) *
      Math.pow(this.gwei, 2)
    );
  }

  getDate(timestamp: string) {
    return new Date(Number.parseInt(timestamp, 10) * 1000);
  }

  getMode(t: Transaction) {
    return this.contract.toLowerCase() === t.to ? 'IN' : 'OUT';
  }

  getValue(value: string) {
    const tmp = Number.parseFloat(value);
    return tmp * this.wei;
  }

  get transactionsLoaded() {
    return this.transactions.length > 0;
  }
}
