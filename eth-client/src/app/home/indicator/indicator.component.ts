import { Component, Input } from '@angular/core';
export enum IndicatorMode {
  IN = 'IN',
  OUT = 'OUT',
}
@Component({
  selector: 'app-indicator',
  templateUrl: './indicator.component.html',
  styleUrls: ['./indicator.component.scss'],
})
export class IndicatorComponent {
  @Input()
  mode: IndicatorMode;

  get text() {
    return this.mode === IndicatorMode.IN ? 'IN' : 'OUT';
  }

  get class() {
    if (this.mode === IndicatorMode.IN) {
      return 'in';
    } else if (this.mode === IndicatorMode.OUT) {
      return 'out';
    }
    return 'hide';
  }
}
