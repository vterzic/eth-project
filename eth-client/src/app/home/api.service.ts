import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransactionDto } from '../shared/models/transaction';
import { Balance } from '../shared/models/balance';

export interface ApiResponse {
  transaction: TransactionDto;
  balance: Balance;
}

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  public getData(address: string, offset: number) {
    return this.http
      .get<ApiResponse>(`eth/etherscan?address=${address}&offset=${offset}`)
      .toPromise();
  }
}
