import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  constructor(private toastController: ToastController) {}
  showToast(message: string, color = 'danger') {
    this.toastController
      .create({
        message,
        color,
        duration: 2000,
        buttons: [{ icon: 'close', role: 'cancel' }],
      })
      .then((t) => t.present());
  }
}
