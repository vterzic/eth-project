import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate',
})
export class TruncatePipe implements PipeTransform {
  public transform(value: string, length: number): string {
    if (!value) {
      return '';
    }
    if (value.length <= length) {
      return value;
    }
    return value.substring(0, length).trim() + '...';
  }
}
