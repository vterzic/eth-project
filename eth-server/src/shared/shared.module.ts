import { HttpModule, Module } from '@nestjs/common';
import { EtherscanProxyService } from './services/etherscan-proxy.service';
import { InfuraProxyService } from './services/infura-proxy.service';

@Module({
  imports: [HttpModule],
  providers: [EtherscanProxyService, InfuraProxyService],
  exports: [EtherscanProxyService, InfuraProxyService],
})
export class SharedModule {}
