import { BadGatewayException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import Web3 from 'web3';

@Injectable()
export class InfuraProxyService {
  private logger = new Logger(InfuraProxyService.name);
  private web3 = new Web3(new Web3.providers.HttpProvider(process.env.INFURA_URL!));
  BLOCK_LIMIT = 9_000_000;

  async getBalance(address: string, blockNum: number | null = null) {
    if (blockNum) {
      // thought this would work
      return await this.web3.eth.getBalance(address, blockNum).catch((err) => {
        throw new BadGatewayException(err);
      });
    }

    return this.web3.eth.getBalance(address).catch((err) => {
      throw new BadGatewayException(err);
    });
  }

  getNewestBlockNumber() {
    return this.web3.eth.getBlockNumber();
  }

  getBlockAt(num: number) {
    return this.web3.eth.getBlock(num, true);
  }

  async getBlockNumberByDate(date: string) {
    const timestamp = new Date(date).setUTCHours(0, 0, 0, 0);
    const lastBlock = await this.getNewestBlockNumber();

    let l = await this.getBlockAt(1);
    let r = await this.getBlockAt(lastBlock);

    // binary search
    while (l.timestamp <= r.timestamp) {
      const m = Math.floor((l.number + r.number) / 2);
      const mBlock = await this.getBlockAt(m);

      if (this.toMidnightDate(mBlock.timestamp) === timestamp) {
        return mBlock;
      } else if (this.toMidnightDate(mBlock.timestamp) < timestamp) {
        l = await this.getBlockAt(m + 1);
      } else {
        r = await this.getBlockAt(m - 1);
      }
      this.logger.warn(`l: ${l.number} | m: ${m} | r: ${r.number}`);
    }

    throw new NotFoundException('NOT FOUND');
  }

  private toMidnightDate(ts: string | number) {
    if (typeof ts === 'string') {
      ts = Number.parseInt(ts);
    }

    return new Date(ts * 1000).setUTCHours(0, 0, 0, 0);
  }

  // tried this out of curiosity
  // turns out to be extremely slow
  async getBalanceFromBlock(address: string, fromBlock: number) {
    const state = this.web3.utils.toBN(0);
    for (let i = fromBlock; i > this.BLOCK_LIMIT; i--) {
      try {
        const block = await this.web3.eth.getBlock(i, true);
        this.logger.log(`traversing block ${i} | time: ${new Date(Number.parseInt(block.timestamp as string) * 1000)}`);

        if (block && block.transactions) {
          for (const t of block.transactions) {
            if (address.toLowerCase() == t.from.toLowerCase()) {
              this.logger.error('FOUND ONE');
              state.sub(this.web3.utils.toBN(t.value));
            }

            if (address.toLowerCase() == t.to?.toLowerCase()) {
              this.logger.error('FOUND ONE');
              state.add(this.web3.utils.toBN(t.value));
            }
          }
        }
      } catch (e) {
        throw new BadGatewayException({ i, e });
      }
    }

    return this.web3.utils.fromWei(state, 'ether');
  }
}
