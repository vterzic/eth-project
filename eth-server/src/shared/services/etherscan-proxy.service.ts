import { HttpService, Injectable, Logger } from '@nestjs/common';

@Injectable()
export class EtherscanProxyService {
  logger = new Logger(EtherscanProxyService.name);

  private API_KEY = process.env.ETHERSCAN_API_KEY;

  private BASE_URL = 'https://api.etherscan.io/api?';

  constructor(private http: HttpService) {}

  async getTransactions(address: string, offset: string) {
    return this.http
      .get(
        `${this.BASE_URL}module=account&action=txlist&address=${address}&startblock=9000000&endblock=latest&page=1&offset=${offset}&sort=asc&apikey=${this.API_KEY}`,
      )
      .toPromise();
  }

  async getBalance(address: string) {
    return this.http
      .get(`${this.BASE_URL}module=account&action=balance&address=${address}&tag=latest&apikey=${this.API_KEY}`)
      .toPromise();
  }
}
