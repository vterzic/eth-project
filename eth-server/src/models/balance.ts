export interface Balance {
  status: string;
  message: string;
  result: string;
}
