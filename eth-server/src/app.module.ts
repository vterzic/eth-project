import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { EthModule } from './eth/eth.module';

@Module({
  imports: [EthModule, ConfigModule.forRoot({ isGlobal: true })],
})
export class AppModule {}
