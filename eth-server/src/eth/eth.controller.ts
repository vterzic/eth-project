import { BadGatewayException, Controller, Get, Logger, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { GetDataDto } from './dto/get-data.dto';
import { EthService } from './eth.service';
@Controller('eth')
@UsePipes(ValidationPipe)
export class EthController {
  logger = new Logger(EthController.name);

  constructor(private readonly ethService: EthService) {}

  @Get('etherscan')
  getDataEtherscan(@Query() dto: GetDataDto) {
    return Promise.all([this.ethService.getEtherscanTransactions(dto), this.ethService.getEtherscanBalance(dto)])
      .then((res) => {
        this.logger.log(res);
        const [transaction, balance] = res;

        if (!transaction.status && !balance.status) {
          throw new BadGatewayException({
            transaction,
            balance,
          });
        }

        return { transaction, balance };
      })
      .catch((err) => {
        throw new BadGatewayException(err);
      });
  }

  @Get('infura')
  getBalanceAtDate(@Query() dto: GetDataDto) {
    return this.ethService.getInfuraBalanceAtDate(dto);
  }
}
