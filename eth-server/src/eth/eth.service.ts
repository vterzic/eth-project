import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InfuraProxyService } from 'src/shared/services/infura-proxy.service';
import { Balance } from '../models/balance';
import { Transaction } from '../models/transaction';
import { EtherscanProxyService } from '../shared/services/etherscan-proxy.service';
import { GetDataDto } from './dto/get-data.dto';
@Injectable()
export class EthService {
  logger = new Logger(EthService.name);

  constructor(
    private readonly etherscanProxy: EtherscanProxyService,
    private readonly infuraProxy: InfuraProxyService,
  ) {}

  async getEtherscanTransactions(dto: GetDataDto) {
    const { address, offset = '10' } = dto;
    const { data } = await this.etherscanProxy.getTransactions(address, offset);

    return data as Transaction;
  }

  async getEtherscanBalance(dto: GetDataDto) {
    const { address } = dto;
    const { data } = await this.etherscanProxy.getBalance(address);

    return data as Balance;
  }

  async getInfuraBalance(dto: GetDataDto) {
    const { address } = dto;

    return await this.infuraProxy.getBalance(address);
  }

  async getInfuraBalanceAtDate(dto: GetDataDto) {
    const { address, date } = dto;

    if (!date) {
      throw new BadRequestException('Date is mandatory field!');
    }

    const block = await this.infuraProxy.getBlockNumberByDate(date);
    const balance = await this.infuraProxy.getBalanceFromBlock(address, block.number);
    return {
      balance,
    };
  }
}
