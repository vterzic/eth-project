import { Module } from '@nestjs/common';
import { EthService } from './eth.service';
import { EthController } from './eth.controller';
import { SharedModule } from 'src/shared/shared.module';

@Module({
  providers: [EthService],
  controllers: [EthController],
  imports: [SharedModule],
})
export class EthModule {}
