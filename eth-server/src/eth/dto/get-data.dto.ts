import { IsISO8601, IsNumberString, IsOptional, IsString } from 'class-validator';

export class GetDataDto {
  @IsString()
  address: string;

  @IsOptional()
  @IsNumberString()
  offset: string;

  @IsOptional()
  @IsISO8601()
  date?: string;
}
